package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.Order;
import fr.unice.polytech.soa1.inike.rest.entities.OrderStatus;
import fr.unice.polytech.soa1.inike.rest.entities.Payment;
import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.exceptions.OrderRequestExecption;
import fr.unice.polytech.soa1.inike.rest.storages.PaymentStorage;
import fr.unice.polytech.soa1.inike.rest.storages.UserStorage;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class OrderStorage {

    private static List<Order> orders = new ArrayList<Order>();

    public static Order createOder(int payId, int idUser){
        Payment pay = PaymentStorage.getPaymentById(payId);
        User user = UserStorage.findUser(idUser);
        Order order = new Order(user,pay);
        orders.add(order);
        return order;
    }

    public static Order getOrderById(int idOrder){
        for(Order o: orders) {
            if (o.getOrderId() == idOrder) {
            return o;
           }
       }
        return null;
    }

    public static Collection<Order> getAllOrders(){
        return  orders;
    }

    public static List<Order> getOrdersByFilter(HashMap<String,Calendar> filters) {
        List<Order> orderList = new ArrayList<Order>();
        for (Order o: orders){
            if(filters.get("date") != null) {
                if(calendarCompartor(filters.get("date"), o.getDate()) != 0) continue;
            }
            if(filters.get("dateFrom") != null) {
                if(calendarCompartor(filters.get("dateFrom"), o.getDate()) > 0) continue;
            }
            if(filters.get("dateTo") != null) {
                if(calendarCompartor(filters.get("dateTo"), o.getDate()) <0) continue;
            }
            orderList.add(o);
        }
        return orderList;
    }

    public static int calendarCompartor(Calendar cal1, Calendar cal2) {
        if(cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
            return 1;
        } else if(cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
            return -1;
        } else if(cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR)) {
            return 1;
        } else if(cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR)) {
            return -1;
        } else {
            return 0;
        }
    }
    public static HashMap<String, Calendar> valideOrderFilter(String request) throws OrderRequestExecption {
        HashMap<String, Calendar> queries = new HashMap<String, Calendar>();
        List<String> filters = new ArrayList<String>(Arrays.asList(new String[]{"date", "dateTo", "dateFrom"}));
        try {
            for (String s : request.split("\\|")) {
                String[] query = s.split("::");
                if(filters.contains(query[0])) {
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(df.parse(query[1]));
                    queries.put(query[0], cal);
                }
            }
        } catch (ParseException e) {
            throw new OrderRequestExecption();
        } catch(Exception e) {
            throw new OrderRequestExecption();
        }
        return queries;
    }

    public static List<Order> getOrdersByUserId(int userId){
        List<Order> orderList = new ArrayList<Order>();
        for (Order o: orders){
            if (o.getUser().getId() == userId){
                orderList.add(o);
            }
        }
        return orderList;
    }

    public static Order getOrderByUserIdByOrderId(int userId, int orderId){

        for (Order o: orders){
            if (o.getUser().getId() == userId && o.getOrderId() == orderId){
                return o;
            }
        }
        return  null;
    }

    public static Order updateOrderStatus(int orderId,OrderStatus os) {
        for (Order o : orders) {
            if (o.getOrderId() == orderId) {
                o.setStatus(os);
                return o;
            }
        }
        return null;
    }
    //status
    public static OrderStatus validUpdateOrderStatus(String request) throws OrderRequestExecption {
        OrderStatus status = null;
        try {
            JSONObject obj = new JSONObject(request);
            status = OrderStatus.valueOf(((String) obj.get("status")).toUpperCase());
            if(status ==null) {
                throw new OrderRequestExecption();
            }
        } catch (Exception e) {
            throw new OrderRequestExecption();
        }
        return status;
    }

}
