package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.exceptions.CartRequestException;
import org.json.JSONObject;

import java.util.HashMap;

public class CartStorage {
    private static HashMap<User, HashMap<Product, Integer>> carts = new HashMap<User, HashMap<Product, Integer>>();

    public static HashMap<Product, Integer> findCartByUserId(int userId) {
        return carts.get(UserStorage.findUser(userId));
    }
    public static void addNewUserCart(User u) {
        carts.put(u, new HashMap<Product, Integer>());
    }
    public static void changeProductQuantityToCart(int userId, int productId, int quantity) {
      HashMap<Product, Integer> cart = findCartByUserId(userId);
      Product product = ProductStorage.findProductById(productId);
        if(quantity == 0) {
            cart.remove(product);
        } else if(quantity > 0) {
                cart.put(product, quantity);
        }
    }

    public static void clearCartByUser(User user) {
        carts.put(user, new HashMap<Product, Integer>());
    }

    public static HashMap<String, Integer> validUpdateCart(String request) throws CartRequestException {
        HashMap<String, Integer> queries = new HashMap<String, Integer>();
        try {
        JSONObject obj = new JSONObject(request);
            Integer productId = Integer.parseInt((String) obj.get("productId"));
            Integer quantity = Integer.parseInt((String) obj.get("quantity"));
            if(quantity < 0) {
                throw new CartRequestException();
            }
            queries.put("productId", productId);
            queries.put("quantity", quantity);
        } catch (Exception ce) {
            throw new CartRequestException();
        }
        return queries;
    }
    static {
        for(User u : UserStorage.findAllUsers()) {
            carts.put(u, new HashMap<Product, Integer>());
        }
    }
}
