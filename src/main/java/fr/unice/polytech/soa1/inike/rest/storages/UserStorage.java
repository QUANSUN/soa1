package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.exceptions.UserRequestException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;

public class UserStorage {

    //this mocks a database
    private static HashMap<String, User> users = new HashMap<String, User>();

    public static void createUser(String name) {
        User user = new User(name);
        users.put(name, user);
        CartStorage.addNewUserCart(user);
    }

    public static User findUser(int userId) {
        User user = null;
        for(User u : users.values()) {
            if(u.getId() == userId) {
                user = u;
            }
        }
        if(user == null) {
            return null;
        } else {
            return user;
        }
    }
    public static String valid(String data) throws UserRequestException {
        try {
            JSONObject obj = new JSONObject(data);
            if(obj.get("name") == null) {
                throw new UserRequestException();
            }
            return (String) obj.get("name");
        }  catch (Exception e) {
            throw new UserRequestException();
        }

    }
    public static User read(String name) {
        return users.get(name);
    }
    public static Collection<User> findAllUsers() {
        return users.values();
    }
    public static void delete(int id) {
        User user = null;
        for(User u : users.values()) {
            if(u.getId() == id) {
                user = u;
                break;
            }
        }
        if(user != null) {
            users.remove(user.getName());
        }
    }
    static {
        UserStorage.createUser("sunquan");
        UserStorage.createUser("renzhou");
    }
}
