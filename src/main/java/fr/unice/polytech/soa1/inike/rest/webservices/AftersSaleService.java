package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.AfterSaleTicket;
import fr.unice.polytech.soa1.inike.rest.storages.AfterSaleTicketStorage;
import fr.unice.polytech.soa1.inike.rest.entities.TicketType;
import fr.unice.polytech.soa1.inike.rest.exceptions.AfterSaleTicketRequestException;
import fr.unice.polytech.soa1.inike.rest.storages.UserStorage;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

@Path("/aftersaletickets")
@Produces(MediaType.APPLICATION_JSON)
public class AftersSaleService {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTicket(String data){
        JSONObject result = new JSONObject();
        HashMap<String, Object> queries = null;
        try {
           queries = AfterSaleTicketStorage.validTicketRequest(data);
        } catch (AfterSaleTicketRequestException e) {
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("message", "The format of the request is invalid!");
            result.put("status", "error");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }
        TicketType ticketType = (TicketType) queries.get("ticketType");
        String tc = (String) queries.get("ticketContent");
        Integer userId = (Integer) queries.get("userId");
        Integer orderId = (Integer) queries.get("orderId");
        if(UserStorage.findUser(userId) == null) {
            result.put("status", "error");
            result.put("code", Response.Status.NOT_FOUND.getStatusCode());
            result.put("message", "The user is not existed!");
            return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
        }
           AfterSaleTicket ticket  = AfterSaleTicketStorage.createTicket(userId, ticketType, tc,orderId);
            JSONObject content = new JSONObject();
            content.put("ticketId", ticket.getTicketId());
            content.put("content", "Your request will be treated!");
            result.put("data", content);
            result.put("code", Response.Status.OK.getStatusCode());
            result.put("status", "success");
            return Response.ok().entity(result.toString()).build();

    }

    @Path("/{ticketId}")
    @GET
    public  Response getTicket(@PathParam("ticketId") int ticketId){

            JSONObject result = new JSONObject();
            JSONObject data = new JSONObject();
            AfterSaleTicket ticket = AfterSaleTicketStorage.getTicketById(ticketId);
            if ( ticket != null) {
                result.put("data",jsonObjectWrapper(ticket));
                result.put("status", "success");
                result.put("code",Response.Status.OK.getStatusCode());
                return  Response.ok().entity(result.toString()).build();
            }else{
                result.put("status", "error");
                result.put("data", data);
                result.put("code", Response.Status.NOT_FOUND);
                return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
            }
    }


    private  JSONObject jsonObjectWrapper(AfterSaleTicket t){
            JSONObject ticketContent = new JSONObject();
            ticketContent.put("ticketId", t.getTicketId());
            ticketContent.put("userId",t.getTicketType());
            ticketContent.put("amount", t.getTicketContent());
            ticketContent.put("date", t.getDate().getTime());
            return ticketContent;

    }

}
