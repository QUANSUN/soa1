package fr.unice.polytech.soa1.inike.rest.entities;

import java.util.Calendar;

public class Order {

    private int orderId;
    private User user;
    private Payment pay;
    private static int count = 1;
    private Calendar date;
    private OrderStatus status;

    //Todo product list
    //Todo order status treated untreated


    public Order(){

        orderId = count++;};

    public OrderStatus getStatus() {
        return status;
    }

    public void setPay(Payment pay) {
        this.pay = pay;
    }

    public Order( User user, Payment pay){
        this.orderId = count++;
        this.user= user;
        this.pay = pay;
        int i = (int) Math.round(Math.random());
        if (i == 0){
        date = Calendar.getInstance();
        } else{
            date = Calendar.getInstance() ;
            date.set(2015,10,4);
        }
        this.status = OrderStatus.ORDERED;


    }


    public int getCount() {
        return count;
    }

    public Payment getPay() {
        return pay;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {

        return user;
    }


    public Calendar getDate(){
        return date;
    }

    public static void setCount(int count) {
        Order.count = count;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setStatus (OrderStatus os){
        this.status = os;
    }

}
