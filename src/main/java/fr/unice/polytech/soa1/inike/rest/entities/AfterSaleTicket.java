package fr.unice.polytech.soa1.inike.rest.entities;

import fr.unice.polytech.soa1.inike.rest.entities.TicketType;
import fr.unice.polytech.soa1.inike.rest.entities.User;

import java.util.Calendar;

public class AfterSaleTicket {
    private User user;
    private int ticketId;
    private fr.unice.polytech.soa1.inike.rest.entities.TicketType TicketType;
    private Calendar date;
    private String TicketContent;
    private int orderId;
    private static int count = 1;

    public AfterSaleTicket(){}

    public AfterSaleTicket(User user,TicketType ticketType,String TicketContent, int orderId){
        this.user =user;
        this.TicketType = ticketType;
        this.TicketContent = TicketContent;
        this.ticketId = count++;
        this.date = Calendar.getInstance();
        this.orderId = orderId;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public TicketType getTicketType() {
        return TicketType;
    }

    public void setTicketType(TicketType ticketType) {
        TicketType = ticketType;
    }

    public String getTicketContent() {
        return TicketContent;
    }

    public void setTicketContent(String ticketContent) {
        TicketContent = ticketContent;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
