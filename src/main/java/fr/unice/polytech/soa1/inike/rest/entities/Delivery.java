package fr.unice.polytech.soa1.inike.rest.entities;

public class Delivery {
    private String address;
    private DeliveryType deliveryType;
    private double deliveryFee;

    public Delivery() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
        if(deliveryType==DeliveryType.EXPRESS) {
            deliveryFee = 19;
        } else if(deliveryType == DeliveryType.NORMAL) {
            deliveryFee = 9;
        }

    }

    public double getDeliveryFee() {
        return deliveryFee;
    }
}
