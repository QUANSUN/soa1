package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.storages.ProductStorage;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/promotion")
@Produces(MediaType.APPLICATION_JSON)
public class PromotionService {

    @GET
    public Response findRecommandProducts() {
        List<Product> prods = ProductStorage.getProductsEnSale();
        JSONObject obj = new JSONObject();
        JSONArray data = new JSONArray();
        for(Product p : prods) {
            JSONObject productContent = new JSONObject();
            productContent.put("id", p.getId());
            productContent.put("name", p.getName());
            productContent.put("price", p.getPrice());
            data.put(productContent);
        }
        obj.put("status", "success");
        obj.put("code", Response.Status.OK.getStatusCode());
        obj.put("data", data);
        obj.put("message", "The products on sales, Come on!!!");
        return Response.ok().entity(obj.toString()).build();
    }
}
