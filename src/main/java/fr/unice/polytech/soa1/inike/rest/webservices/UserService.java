package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.Order;
import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.exceptions.CartRequestException;
import fr.unice.polytech.soa1.inike.rest.exceptions.UserRequestException;
import fr.unice.polytech.soa1.inike.rest.storages.CartStorage;
import fr.unice.polytech.soa1.inike.rest.storages.OrderStorage;
import fr.unice.polytech.soa1.inike.rest.storages.ProductStorage;
import fr.unice.polytech.soa1.inike.rest.storages.UserStorage;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

// Here we generate JSON data from scratch, one should use a framework instead
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    @GET
    public Response findAllUsers() {
        Collection<User> users = UserStorage.findAllUsers();
        JSONObject obj = new JSONObject();
        obj.put("status", "success");
        JSONArray data = new JSONArray();
        for(User u: users) {
            JSONObject userContent = new JSONObject();
            userContent.put("name", u.getName());
            userContent.put("id", u.getId());
            data.put(userContent);
        }
        obj.put("data", data);
        obj.put("code", Response.Status.OK.getStatusCode());
        return Response.ok().entity(obj.toString()).build();
    }

    @Path("/{id}")
    @GET
    public Response findUserById(@PathParam("id") int id) {
        User user = UserStorage.findUser(id);
        JSONObject obj = new JSONObject();
        if(user != null) {
            obj.put("status", "success");
            JSONObject userContent = new JSONObject();
            userContent.put("id", user.getId());
            userContent.put("name", user.getName());
            obj.put("data", userContent);
            obj.put("code", Response.Status.OK.getStatusCode());
            return Response.ok().entity(obj.toString()).build();
        } else {
            obj.put("status", "error");
            obj.put("message", "The ressource is not found!");
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(String data) {
        JSONObject result = new JSONObject();
        JSONObject obj = null;
        String name = null;
        try {
            name = UserStorage.valid(data);
            obj = new JSONObject(data);
        } catch (UserRequestException e) {
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("status", "error");
            result.put("message", "The format of the request is not correct!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }
        if(UserStorage.read(name) != null) {
            result.put("status", "error");
            result.put("code", Response.Status.CONFLICT.getStatusCode());
            result.put("message", "The username is already existed!");
            return Response.status(Response.Status.CONFLICT).entity(result.toString()).build();
        }
        UserStorage.createUser(name);
        result.put("status", "success");
        result.put("code", Response.Status.OK.getStatusCode());
        result.put("message", "The user is created!");
        return Response.ok().entity(result.toString()).build();
    }

    @Path("/{id}")
    @DELETE
    public Response deleteUser(@PathParam("id") int id) {
        User user = UserStorage.findUser(id);
        JSONObject obj = new JSONObject();
        if(user == null) {
            obj.put("status", "error");
            obj.put("message", "The ressource is not found!");
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
        UserStorage.delete(id);
        obj.put("status", "success");
        obj.put("code", Response.Status.OK.getStatusCode());
        obj.put("message", "The ressource is deleted!");
        return Response.ok().entity(obj.toString()).build();
    }

    @Path("/{id}/cart")
    @GET
    public Response checkCart(@PathParam("id") int id) {
        JSONObject obj = new JSONObject();
        HashMap<Product, Integer> cart = CartStorage.findCartByUserId(id);
        if (cart != null) {
            obj.put("code", Response.Status.OK.getStatusCode());
            obj.put("status", "success");
            JSONObject userContent = new JSONObject();
            User u = UserStorage.findUser(id);
            JSONObject dataContent = new JSONObject();
            dataContent.put("userId", u.getId());
            dataContent.put("username", u.getName());
            JSONArray products = new JSONArray();
            double totalAmount = 0;
            for(Product p : cart.keySet()) {
                JSONObject prodJSON = jsonObjectWrapper(p);
                prodJSON.put("quantity", cart.get(p));
                totalAmount += cart.get(p) * p.getPrice() * p.getDiscount();
                products.put(prodJSON);
            }
            dataContent.put("products", products);
            dataContent.put("total", totalAmount);
            obj.put("data", dataContent);
        } else {
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            obj.put("status", "error");
            obj.put("message", "The ressource is not found");
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
        obj.put("code", Response.Status.OK.getStatusCode());
        obj.put("status", "success");
        return Response.ok().entity(obj.toString()).build();
    }

    @Path("/{id}/cart")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modifyCart(@PathParam("id") int id, String data) {
        JSONObject obj = new JSONObject();
        HashMap<String, Integer> queries = null;
        try {
            queries = CartStorage.validUpdateCart(data);
        } catch (CartRequestException ce) {
            obj.put("status", "error");
            obj.put("message", "The format of your request is invalid!");
            obj.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(obj.toString()).build();
        }

        int productId = (Integer)queries.get("productId");
        int quantity = (Integer) queries.get("quantity");
        if(UserStorage.findUser(id) == null || ProductStorage.findProductById(productId) == null) {
            obj.put("status", "error");
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            obj.put("message", "The user or the product is not found!");
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        } else {
            CartStorage.changeProductQuantityToCart(id, productId, quantity);
            obj.put("status", "success");
            obj.put("code", Response.Status.OK.getStatusCode());
            obj.put("message", "The cart is modified!");
            return Response.ok().entity(obj.toString()).build();
        }
    }

    @Path("/{userId}/orders")
    @GET
    public Response getUserOrders(@PathParam("userId") int userId){
        JSONObject result = new JSONObject();
        JSONArray data = new JSONArray();
        User user = UserStorage.findUser(userId);
        if (user != null){
            List<Order> userOrders = OrderStorage.getOrdersByUserId(userId);
            if (userOrders != null && userOrders.size() != 0 ){
                for (Order o:userOrders){
                    data.put(jsonObjectLinkWrapper(o));
                }
                result.put("status","success");
                result.put("code",Response.Status.OK.getStatusCode());
                result.put("data", data);
                return Response.ok().entity(result.toString()).build();
            }else {
                result.put("status", "success");
                result.put("code", Response.Status.NO_CONTENT);
                return Response.status(Response.Status.NO_CONTENT).entity(result.toString()).build();
            }

        }else{
            result.put("status", "error");
            result.put("code", Response.Status.NOT_FOUND);
            result.put("message", "The user is not found!");
            return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
        }

    }

    private JSONObject jsonObjectLinkWrapper(Order o){

        JSONObject orderContent = new JSONObject();
        orderContent.put("orderId", o.getOrderId());
        orderContent.put("date",o.getDate().getTime());

        return orderContent;
    }

    private JSONObject jsonObjectWrapper(Product p) {
        JSONObject productContent = new JSONObject();
        productContent.put("id", p.getId());
        productContent.put("name", p.getName());
        productContent.put("color", p.getColor());
        productContent.put("type", p.getType());
        productContent.put("price", p.getPrice());
        productContent.put("gender", p.getGender());
        productContent.put("discount", p.getDiscount());
        productContent.put("logo", p.getLogo());
        return productContent;
    }

}
