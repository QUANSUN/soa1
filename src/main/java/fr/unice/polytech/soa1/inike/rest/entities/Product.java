package fr.unice.polytech.soa1.inike.rest.entities;

public class Product {
    private String name;
    private ProductType type;
    private double price;
    private Gender gender;
    private static int count = 1;
    private int id;
    private double discount = 1.0;
    private String logo;
    private int size;
    private Color color;


    public Product() {
        id = count++;
    }
    public Product(String name, ProductType type, double price, Gender gender,
                   Color color, String logo, int size, double discount) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.gender = gender;
        this.id = count++;
        this.color = color;
        this.logo = logo;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Product.count = count;
    }

    public int getId() {
        return id;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", price=" + price +
                ", gender=" + gender +
                ", id=" + id +
                ", discount=" + discount +
                ", logo='" + logo + '\'' +
                ", size=" + size +
                ", color=" + color +
                '}';
    }
}
