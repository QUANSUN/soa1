package fr.unice.polytech.soa1.inike.rest.entities;

public enum  OrderStatus  {
    ORDERED,SHIPPING,SHIPPED
}
