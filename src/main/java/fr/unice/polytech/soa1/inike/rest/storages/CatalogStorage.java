package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.ProductType;

import java.util.Arrays;
import java.util.List;

public class CatalogStorage {

    public static List<ProductType> getAllProductType() {
        return Arrays.asList(ProductType.values());
    }
}
