package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.*;
import fr.unice.polytech.soa1.inike.rest.exceptions.PaymentRequestException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;

public class PaymentStorage {

    //payment
    private static HashMap<User,Payment> payments = new HashMap<User,Payment>();
    public static Payment createPayment(Long bankCard, User user, HashMap<Product, Integer> cart, String address, DeliveryType deliveryType){
        Delivery delivery = new Delivery();
        delivery.setAddress(address);
        delivery.setDeliveryType(deliveryType);
        double amount = 0;
        for(Product p : cart.keySet()) {
            amount += p.getPrice() * cart.get(p)* p.getDiscount();
        }
        amount += delivery.getDeliveryFee();
        Payment payment = new Payment(user, bankCard, amount, delivery);
        payments.put(user, payment);
        return payment;
    }
    public static Payment getPaymentById(int payId){
        for(Payment p : payments.values()) {
            if(p.getId() == payId) {
                return p;
            }
        }
        return null;
    }

    //bankCard, address, deliveryType, userId
    public static HashMap<String, Object> validPaymentRequest(String request) throws PaymentRequestException {
        HashMap<String, Object> queries = new HashMap<String, Object>();
        try {
            JSONObject obj = new JSONObject(request);
            int userId = Integer.parseInt((String) obj.get("userId"));
            String address = (String) obj.get("address");
            Long bankCard = Long.parseLong((String) obj.get("bankCard"));
            DeliveryType delivery = DeliveryType.valueOf(((String)obj.get("deliveryType")).toUpperCase());
            if(address==null || bankCard == null || delivery == null) {
                throw new PaymentRequestException();
            }
            queries.put("userId", userId);
            queries.put("address", address);
            queries.put("bankCard", bankCard);
            queries.put("delivery", delivery);
        } catch (Exception e) {
            throw  new PaymentRequestException();
        }
        return queries;
    }

    public static Collection<Payment> getPay(){
        return payments.values();
    }
}
