package fr.unice.polytech.soa1.inike.rest.entities;

public enum DeliveryType {
    NORMAL, EXPRESS
}
