package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.storages.OrderStorage;
import fr.unice.polytech.soa1.inike.rest.entities.Order;
import fr.unice.polytech.soa1.inike.rest.entities.OrderStatus;
import fr.unice.polytech.soa1.inike.rest.exceptions.OrderRequestExecption;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Path("/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderService {


    //date, dateFrom, dateTo
    @GET
    public Response findOrders(@QueryParam("filter") String filter) {

        JSONObject result = new JSONObject();
        JSONArray data = new JSONArray();
        if (filter == null) {
            return getAllOrders();
        } else {
            HashMap<String, Calendar> queries = null;
            try {
                queries = OrderStorage.valideOrderFilter(filter);
            } catch (OrderRequestExecption e) {
                result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
                result.put("status", "error");
                result.put("message", "The format of the request is invalid!");
                return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
            }
            if (queries == null) {
                return getAllOrders();
            }
            List<Order> orders = OrderStorage.getOrdersByFilter(queries);
            for (Order o : orders) {
                data.put(jsonObjectLinkWrapper(o));
            }
            result.put("data", data);
            result.put("status", "success");
            result.put("code", Response.Status.OK.getStatusCode());
            return Response.ok().entity(result.toString()).build();
        }
    }

    private Response getAllOrders() {
        JSONObject result = new JSONObject();
        JSONArray data = new JSONArray();
        Collection<Order> orders = OrderStorage.getAllOrders();
        for (Order o : orders) {
            data.put(jsonObjectLinkWrapper(o));
        }
        result.put("data", data);
        result.put("status", "success");
        result.put("code", Response.Status.OK.getStatusCode());
        return Response.ok().entity(result.toString()).build();
    }

    @Path("/{orderId}")
    @GET
    public Response getOrderById( @PathParam("orderId") int orderId){
        JSONObject result = new JSONObject();
        JSONObject data = new JSONObject();
        Order order = OrderStorage.getOrderById(orderId);
        if (order != null) {
            result.put("data",jsonObjectWrapper(order));
            result.put("status", "success");
            result.put("code",Response.Status.OK.getStatusCode());
            return  Response.ok().entity(result.toString()).build();
        }else{
            result.put("status", "error");
            result.put("data", data);
            result.put("code", Response.Status.NOT_FOUND);
            return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
        }
    }

    @Path("/{orderId}/status")
    @GET
    public Response getDelivery(@PathParam("orderId") int orderId){
        JSONObject result = new JSONObject();
        JSONObject data = new JSONObject();
        Order order = OrderStorage.getOrderById(orderId);
        if (order != null){
            data = jsonObjectStatusWrapper(order);
            result.put("data", data);
            result.put("status", "success");
            result.put("code",Response.Status.OK.getStatusCode());
            return  Response.ok().entity(result.toString()).build();
        }else {
            result.put("status", "error");
            result.put("data", data);
            result.put("code", Response.Status.NOT_FOUND);
            return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
        }

    }

    @Path("/{orderId}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    @PUT
   public Response changeDeliveryStatus(@PathParam("orderId") int orderId,
                                         String data){
        OrderStatus status = null;
        JSONObject result = new JSONObject();
        try {
            status = OrderStorage.validUpdateOrderStatus(data);
        } catch (OrderRequestExecption e) {
            result.put("status", "error");
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("message", "The format of the request is invalid!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }

       Order order = OrderStorage.updateOrderStatus(orderId, status);
       if (order != null){
           JSONObject orderContent = jsonObjectStatusWrapper(order);
            result.put("data", data);
            result.put("status", "success");
            result.put("code",Response.Status.OK.getStatusCode());
            return  Response.ok().entity(result.toString()).build();
        }else {
            result.put("status", "error");
            result.put("message", "The order is not found!");
            result.put("code", Response.Status.NOT_FOUND);
            return Response.status(Response.Status.NOT_FOUND).entity(result.toString()).build();
        }

    }

    private JSONObject jsonObjectWrapper(Order o){

        JSONObject orderContent = new JSONObject();
        orderContent.put("orderId", o.getOrderId());
        orderContent.put("userId",o.getUser().getId());
        orderContent.put("amount", o.getPay().getAmount());
        orderContent.put("date", o.getDate().getTime());
        return orderContent;
    }

    private JSONObject jsonObjectStatusWrapper(Order o){
        JSONObject orderContent = new JSONObject();
        orderContent.put("orderId", o.getOrderId());
        orderContent.put("adsress", o.getPay().getDelivery().getAddress());
        orderContent.put("deliveryStatus",o.getStatus());
        return orderContent;
    }

    private JSONObject jsonObjectLinkWrapper(Order o){
        JSONObject orderContent = new JSONObject();
        orderContent.put("orderId", o.getOrderId());
        orderContent.put("date",o.getDate().getTime());
        return orderContent;
    }
}
