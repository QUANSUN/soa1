package fr.unice.polytech.soa1.inike.rest.entities;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum(String.class)
public enum ProductType {
    BASKETBALL,TENNIS, FOOTBALL
}
