package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.AfterSaleTicket;
import fr.unice.polytech.soa1.inike.rest.entities.TicketType;
import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.exceptions.AfterSaleTicketRequestException;
import fr.unice.polytech.soa1.inike.rest.storages.UserStorage;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AfterSaleTicketStorage {
    private static List<AfterSaleTicket> tickets = new ArrayList<AfterSaleTicket>();

    public static AfterSaleTicket createTicket(int userId,TicketType tt, String tc, int orderId){
        User user = UserStorage.findUser(userId);
       AfterSaleTicket t = new AfterSaleTicket(user,tt,tc,orderId);
        tickets.add(t);
        return  t;
    }

    //ticketType, ticketContent, userId
    public static HashMap<String, Object> validTicketRequest(String request) throws AfterSaleTicketRequestException {
        HashMap<String, Object> queries = new HashMap<String, Object>();
        try {
            JSONObject obj = new JSONObject(request);
            TicketType ticketType = TicketType.valueOf(((String)obj.get("ticketType")).toUpperCase());
            String tickerContent = (String) obj.get("ticketContent");
            Integer userId = Integer.parseInt((String) obj.get("userId"));
            if(tickerContent == null || ticketType == null || userId == null) {
                throw  new AfterSaleTicketRequestException();
            }
            queries.put("userId", userId);
            queries.put("ticketType", ticketType);
            queries.put("ticketContent", tickerContent);
        } catch (Exception e) {
            throw new AfterSaleTicketRequestException();
        }
        return queries;
    }

    public static AfterSaleTicket getTicketById(int ticketId){
        for (AfterSaleTicket t: tickets) {
          if ( t.getTicketId() == ticketId){ return t;}
        }
        return null;
    }

}
