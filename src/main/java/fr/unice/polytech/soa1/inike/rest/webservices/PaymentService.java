package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.Order;
import fr.unice.polytech.soa1.inike.rest.storages.OrderStorage;
import fr.unice.polytech.soa1.inike.rest.entities.DeliveryType;
import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.entities.User;
import fr.unice.polytech.soa1.inike.rest.entities.Payment;
import fr.unice.polytech.soa1.inike.rest.exceptions.PaymentRequestException;
import fr.unice.polytech.soa1.inike.rest.storages.CartStorage;
import fr.unice.polytech.soa1.inike.rest.storages.PaymentStorage;
import fr.unice.polytech.soa1.inike.rest.storages.UserStorage;
import org.json.JSONObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;


// Here we generate JSON data from scratch, one should use a framework instead
@Path("/payments")
@Produces(MediaType.APPLICATION_JSON)
public class PaymentService {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createNewPayment(String data) {
        JSONObject result = new JSONObject();
        HashMap<String, Object> queries = null;
        try{
            queries = PaymentStorage.validPaymentRequest(data);
        } catch (PaymentRequestException e) {
            result.put("status", "error");
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("message", "The format of the request is invalid!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }
        int userId = (Integer)queries.get("userId");
        User user = UserStorage.findUser(userId);
        if(user==null) {
            result.put("status", "error");
            result.put("code", Response.Status.FORBIDDEN.getStatusCode());
            result.put("message", "The user is not existed!");
            return Response.status(Response.Status.FORBIDDEN).entity(result.toString()).build();
        }
        HashMap<Product, Integer> cart = CartStorage.findCartByUserId(userId);
        if(cart.size()==0) {
            result.put("status", "error");
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("message", "The cart is empty!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }
        Long bankCard = (Long)queries.get("bankCard");
        //The valid bank card is a pair number
        if(bankCard % 2 !=0) {
            result.put("status", "error");
            result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            result.put("message", "The bank card is invalid!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
        }


        DeliveryType deliveryType = (DeliveryType) queries.get("deliveryType");
        String address = (String) queries.get("address");
        JSONObject jobj = new JSONObject(data);
            Payment payment = PaymentStorage.createPayment(bankCard, user, cart, address, deliveryType);
            CartStorage.clearCartByUser(user);
            Order order = OrderStorage.createOder(payment.getId(), userId);
            //todo productlist
            result.put("data",jsonObjectWrapper(order));
            result.put("code", Response.Status.OK.getStatusCode());
            result.put("status", "success");
            return Response.ok().entity(result.toString()).build();
    }

    private JSONObject jsonObjectWrapper(Order o){
        JSONObject payContent = new JSONObject();
        Calendar calendar = o.getPay().getCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        payContent.put("time", sdf.format(calendar.getTime()));
        payContent.put("orderId", o.getOrderId());
        return payContent;

    }
}
