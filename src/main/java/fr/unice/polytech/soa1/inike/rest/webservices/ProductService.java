package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.Color;
import fr.unice.polytech.soa1.inike.rest.entities.Gender;
import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.entities.ProductType;
import fr.unice.polytech.soa1.inike.rest.exceptions.ProductRequestException;
import fr.unice.polytech.soa1.inike.rest.storages.ProductStorage;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductService {

    @GET
    public Response findProductsWithFilter(@QueryParam("filter") String filter) {
        JSONObject obj = new JSONObject();
        if(filter==null) {
            JSONArray data = new JSONArray();
            for(Product p : ProductStorage.findAllProducts()) {
                data.put(jsonObjectWrapper(p));
            }
            obj.put("data", data);
            obj.put("status", "success");
            obj.put("code", Response.Status.OK.getStatusCode());
            return Response.ok().entity(obj.toString()).build();
        } else {
            HashMap<String, Object> queries = null;
            try {
                queries = ProductStorage.valid(filter);
            } catch (ProductRequestException e) {
                obj.put("code", Response.Status.BAD_REQUEST.getStatusCode());
                obj.put("status", "error");
                obj.put("message", "The request is invalid!");
                return Response.status(Response.Status.BAD_REQUEST).entity(obj.toString()).build();
            }
            JSONArray data = new JSONArray();
            for (Product p : ProductStorage.findProductsWithFilter(queries)) {
                data.put(jsonObjectWrapper(p));
            }
            obj.put("data", data);
            obj.put("status", "success");
            obj.put("code", Response.Status.OK.getStatusCode());
            return Response.ok().entity(obj.toString()).build();
        }
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createProduct(String data) {
        JSONObject result = new JSONObject();
        HashMap<String, Object> queries = null;
         try {
             queries = ProductStorage.validCreateProduct(data);
         } catch (ProductRequestException p) {
             result.put("status", "error");
             result.put("code", Response.Status.BAD_REQUEST.getStatusCode());
             result.put("message", "The format of the request in invalid!");
             return Response.status(Response.Status.BAD_REQUEST).entity(result.toString()).build();
         }
        String name = (String) queries.get("name");
        Color color = (Color) queries.get("color");
        ProductType type = (ProductType) queries.get("type");
        Gender gender = (Gender) queries.get("gender");
        String logo = (String) queries.get("logo");
        if(ProductStorage.read(name, type, color, gender, logo) != null) {
            result.put("status", "error");
            result.put("message", "The product is already existed!");
            result.put("code", Response.Status.CONFLICT.getStatusCode());
            return Response.status(Response.Status.CONFLICT).entity(result.toString()).build();
        } else {
            double price = (Double) queries.get("price");
            int size = (Integer) queries.get("size");
            double discount = (Double) queries.get("discount");
            Product product = ProductStorage.createProduct(name, type, price, gender, color, logo, size, discount);
            result.put("status", "success");
            result.put("message", "The operation is successed!");
            result.put("code", Response.Status.OK.getStatusCode());
            return Response.ok().entity(result.toString()).build();
        }
    }
    @Path("/{id}")
    @DELETE
    public Response deleteProductById(@PathParam("id") int id) {
        Product product = ProductStorage.findProductById(id);
        JSONObject obj = new JSONObject();
        if(product == null) {
            obj.put("status", "error");
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            obj.put("message", "The ressource is not found!");
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
        ProductStorage.delete(id);
        obj.put("code", Response.Status.OK.getStatusCode());
        obj.put("status", "success");
        obj.put("message", "The ressource is deleted!");
        return Response.ok().entity(obj.toString()).build();
    }
    @Path("/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateProductById(@PathParam("id") int id, String data) {
        JSONObject obj = new JSONObject();
        HashMap<String, Object> queries = null;
        try {
            queries  = ProductStorage.validUpdateProduct(data);
        } catch (ProductRequestException e) {
            obj.put("status", "error");
            obj.put("code", Response.Status.BAD_REQUEST.getStatusCode());
            obj.put("message", "The format of the request is invalid!");
            return Response.status(Response.Status.BAD_REQUEST).entity(obj.toString()).build();
        }
        Product product = ProductStorage.findProductById(id);
        if(product == null) {
            obj.put("status", "error");
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
        JSONObject queryContent = new JSONObject(data);
        String name = (String)queryContent.get("name");
        Double price = (Double)queries.get("price");
        double discount= (Double)queries.get("discount");
        Product resultProduct = ProductStorage.update(id, name, price, discount);
        obj.put("code", Response.Status.OK.getStatusCode());
        obj.put("status", "success");
        obj.put("message", "The ressource is updated!");
        return Response.ok().entity(obj.toString()).build();
    }

    @Path("/{id}")
    @GET
    public Response findProductById(@PathParam("id") int id) {
        Product product = ProductStorage.findProductById(id);
        JSONObject obj = new JSONObject();
        JSONObject productContent = new JSONObject();
        if(product != null) {
            productContent.put("id", product.getId());
            productContent.put("name", product.getName());
            productContent.put("color", product.getColor());
            productContent.put("type", product.getType());
            productContent.put("price", product.getPrice());
            productContent.put("gender", product.getGender());
            productContent.put("discount", product.getDiscount());
            productContent.put("logo", product.getLogo());
            obj.put("data", productContent);
            obj.put("code", Response.Status.OK.getStatusCode());
            obj.put("status", "success");
            return Response.ok().entity(obj.toString()).build();
        } else {
            obj.put("data", productContent);
            obj.put("code", Response.Status.NOT_FOUND.getStatusCode());
            obj.put("message", "The ressource is not found!");
            obj.put("status", "error");
            return Response.status(Response.Status.NOT_FOUND).entity(obj.toString()).build();
        }
    }

    private JSONObject jsonObjectWrapper(Product p) {
        JSONObject productContent = new JSONObject();
        productContent.put("id", p.getId());
        productContent.put("name", p.getName());
        productContent.put("price", p.getPrice());
        return productContent;
    }

}
