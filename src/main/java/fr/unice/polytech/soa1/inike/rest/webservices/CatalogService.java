package fr.unice.polytech.soa1.inike.rest.webservices;

import fr.unice.polytech.soa1.inike.rest.entities.ProductType;
import fr.unice.polytech.soa1.inike.rest.storages.CatalogStorage;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import java.util.List;

@Path("/catalogs")
@Produces(MediaType.APPLICATION_JSON)
public class CatalogService {

    @GET
    public Response getAllCatalogs(){
        JSONObject result = new JSONObject();
        JSONArray data = new JSONArray();

        List<ProductType> list = CatalogStorage.getAllProductType();
        for(ProductType p:list){
            data.put(p);
        }
            result.put("data", data);
            result.put("status", "success");
            result.put("code",Response.Status.OK.getStatusCode());
            return  Response.ok().entity(result.toString()).build();

    }

}
