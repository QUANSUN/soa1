package fr.unice.polytech.soa1.inike.rest.storages;

import fr.unice.polytech.soa1.inike.rest.entities.Product;
import fr.unice.polytech.soa1.inike.rest.entities.ProductType;
import fr.unice.polytech.soa1.inike.rest.entities.Color;
import fr.unice.polytech.soa1.inike.rest.entities.Gender;
import fr.unice.polytech.soa1.inike.rest.exceptions.ProductRequestException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ProductStorage {
    private static HashMap<String, List<Product>> products = new HashMap<String, List<Product>>();

    public static Product createProduct2(String name, String type, double price, String gender,
                                        String color, String logo, int size, double discount) {
        Product product = new Product(name,  ProductType.valueOf(type), price, Gender.valueOf(gender),
                Color.valueOf(color), logo, size, discount);
        List<Product> prods = products.get(name);
        if(prods != null) {
            prods.add(product);
        } else {
            prods = new ArrayList<Product>();
            prods.add(product);
        }
        products.put(name, prods);
        return product;
    }
    public static Product createProduct(String name,  ProductType type, double price, Gender gender, Color color, String logo, int size, double discount) {
        Product p = new Product();
            p.setName(name);
            p.setType(type);
            p.setPrice(price);
            p.setGender(gender);
            p.setColor(color);
            p.setLogo(logo);
            p.setSize(size);
            p.setDiscount(discount);
            List<Product> prods = products.get(name);
            if(prods != null) {
                prods.add(p);
            } else {
                prods = new ArrayList<Product>();
                prods.add(p);
            }
            products.put(name, prods);
        return p;
    }
    public static HashMap<String, Object> valid(String filter) throws ProductRequestException {
        HashMap<String, Object> queries = new HashMap<String, Object>();
        try {
            for (String s : filter.split("\\|")) {
                String[] query = s.split("::");
                if(query[0].equals("color")) {
                    queries.put(query[0], Color.valueOf(query[1].toUpperCase()));
                } else if(query[0].equals("type")) {
                    queries.put("type", ProductType.valueOf(query[1].toUpperCase()));
                } else if(query[0].equals("gender")) {
                    queries.put(query[0], Gender.valueOf(query[1].toUpperCase()));
                } else if(query[0].equals("minPrice")) {
                    queries.put(query[0], Double.parseDouble((String) query[1]));
                } else if(query[0].equals("maxPrice")) {
                    queries.put(query[0], Double.parseDouble((String) query[1]));
                } else if(query[0].equals("name")) {
                    queries.put(query[0], (String) query[1]);
                }
            }
        } catch(Exception e) {
             throw new ProductRequestException();
        }
        return queries;
    }

    public static HashMap<String, Object> validCreateProduct(String request) throws ProductRequestException {
        HashMap<String, Object> queries = new HashMap<String, Object>();
        try {
            JSONObject obj = new JSONObject(request);
            String name = (String) obj.get("name");
            Color color = Color.valueOf(((String) obj.get("color")).toUpperCase());
            ProductType type = ProductType.valueOf(((String) obj.get("type")).toUpperCase());
            Gender gender = Gender.valueOf(((String) obj.get("gender")).toUpperCase());
            Double price = Double.parseDouble((String) obj.get("price"));
            Double discount = Double.parseDouble((String) obj.get("discount"));
            Integer size = Integer.parseInt((String) obj.get("size"));
            String logo = (String) obj.get("logo");
            if(name == null || color == null || gender == null || type == null || price <= 0 || discount > 1 || discount <= 0 || size <= 0) {
                throw new ProductRequestException();
            }
            queries.put("name", name);
            queries.put("color", color);
            queries.put("type", type);
            queries.put("gender", gender);
            queries.put("price", price);
            queries.put("discount", discount);
            queries.put("size", size);
            queries.put("logo", logo);
        } catch ( Exception e) {
            throw new ProductRequestException();
        }
        return queries;
    }

    //price, discount, name
    public static HashMap<String, Object> validUpdateProduct(String request) throws ProductRequestException {
        HashMap<String, Object> queries = new HashMap<String, Object>();
        try {
            JSONObject obj = new JSONObject(request);
            if(obj.get("price") != null) {
                Double price = Double.parseDouble((String) obj.get("price"));
                if(price <= 0) {
                    throw new ProductRequestException();
                }
                queries.put("price", price);
            }
            if(obj.get("discount") !=null) {
                Double discount = Double.parseDouble((String) obj.get("discount"));
                if(discount <= 0 || discount > 1) {
                    throw  new ProductRequestException();
                }
                queries.put("discount", discount);
            }
            if(obj.get("name")!= null) {
                String name = (String) obj.get("name");
                queries.put("name", name);
            }
        } catch(Exception e) {
            throw new ProductRequestException();
        }

        return queries;
    }

    public static List<Product> getProductsEnSale() {
        List<Product> prods = new ArrayList<Product>();
        for(List<Product> lp : products.values())
            for(Product p : lp) {
                if(p.getDiscount() < 1) {
                    prods.add(p);
                }
            }
        return prods;
    }
    public static Product findProductById(int id) {
        for(List<Product> prods : products.values())
            for (Product p : prods) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    public static List<Product> findProductsWithFilter(HashMap<String, Object> filters) {
        Collection<List<Product>> prods = products.values();
        List<Product> results = new ArrayList<Product>();
        for(List<Product> pds : prods)
            for(Product p : pds){
            if(filters.get("name")!= null && !p.getName().toLowerCase().contains(((String) filters.get("name")).toLowerCase()))
                continue;
            if (filters.get("color") != null && !p.getColor().name().toLowerCase().equals(((Color)filters.get("color")).name().toLowerCase()))
                continue;
            if(filters.get("type") != null && !p.getType().equals((ProductType)filters.get("type"))) continue;
            if(filters.get("gender") != null && !p.getGender().equals((Gender)filters.get("gender"))) continue;
            if(filters.get("minPrice") !=null && p.getPrice() < (Double)filters.get("minPrice")) continue;
            if(filters.get("maxPrice") !=null && p.getPrice() > (Double)filters.get("maxPrice")) continue;
            results.add(p);
        }
        return results;
    }

    public static List<Product> findAllProducts() {
        List<Product> prods = new ArrayList<Product>();
        for(List<Product> lp : products.values())
            for(Product p : lp) {
                prods.add(p);
            }
        return prods;
    }

    public static Product read(String name, ProductType type, Color color, Gender gender, String logo) {
        List<Product> lp = products.get(name);
        if(lp == null || lp.size()==0) return null;
        for(Product p : lp) {
            if (p.getType().name().equals(type.name()) && p.getColor().name().equals(color.name()) && p.getGender().equals(gender.name()) &&
                    p.getLogo().equals("logo")) {
                return p;
            }
        }
        return null;
    }

    public static Product update(int id, String name, double price, double discount) {
        Product product = null;
        for(List<Product> lp : products.values())
            for(Product p : lp){
            if(p.getId() == id) {
                product = p;
                break;
            }
        }
            product.setPrice(price);
            product.setDiscount(discount);
        List<Product> prods = products.get(product.getName());
        prods.remove(product);
        products.put(product.getName(), prods);
        product.setName(name);
        List<Product> prods2 = products.get(name);
        if(prods2 != null) {
            prods2.add(product);
        } else {
            prods2 = new ArrayList<Product>();
            prods2.add(product);
        }
        products.put(name, prods2);
        return product;
    }
    public static void delete(int id) {
        Product product = null;
        for(List<Product> lp : products.values())
            for(Product p : lp){
            if(p.getId() == id) {
                String name = p.getName();
                lp.remove(p);
                products.put(name, lp);
                break;
            }
        }
    }
    static {
        ProductStorage.createProduct2("running man", "FOOTBALL", 100, "MALE", "BLACK", "yoyo.jpg", 43, 1);
        ProductStorage.createProduct2("wokaup",  "BASKETBALL", 200, "FEMALE", "YELLOW", "qq.jpg", 38, 0.9);
        ProductStorage.createProduct2("sakura",  "TENNIS", 150, "MALE", "GREEN", "federa.jpg", 45, 0.99);
        ProductStorage.createProduct2("spider",  "FOOTBALL", 80, "MALE", "BLUE", "messi.jpg", 43, 0.75);
    }
}
