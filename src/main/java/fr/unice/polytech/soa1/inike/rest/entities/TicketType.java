package fr.unice.polytech.soa1.inike.rest.entities;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public enum TicketType {
    REFUND, EXCHANGE
}
