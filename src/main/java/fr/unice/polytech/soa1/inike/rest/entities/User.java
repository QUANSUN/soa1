package fr.unice.polytech.soa1.inike.rest.entities;


public class User {
    private static int count = 1;
    private  int id;
    private String name;

    public User(String name) {
        this.name = name;
        this.id = count++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
