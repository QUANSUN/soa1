package fr.unice.polytech.soa1.inike.rest.entities;

import java.util.Calendar;

public class Payment {
    private int id;
    private static int count = 1;
    private User user;
    private Long bankCard;
    private double amount;
    private Delivery delivery;
    private Calendar calendar;

    public Payment() {
        id = count++;
    }
    public Payment(User user,Long bankCard, double amount,Delivery delivery ){
        id = count++;
        this.user = user;
        this.bankCard = bankCard;
        this.amount = amount;
        this.delivery = delivery;
        this.calendar = Calendar.getInstance();
    }

    public double getAmount() {
        return amount;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getBankCard() {
        return bankCard;
    }

    public void setBankCard(Long bankCard) {
        this.bankCard = bankCard;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Calendar getCalendar() {
        return calendar;
    }
}
